#!/bin/sh

source ./.env

podman pod create \
	-p 8080:80 \
	--name pod-writefreely

podman run \
	--name "writefreely-db" \
	--pod "pod-writefreely" \
	--volume "writefreely-db:/var/lib/mysql/data:z" \
	--env MYSQL_DATABASE="writefreely" \
	--env MYSQL_ROOT_PASSWORD="$WRITEFREELY_MYSQL_ROOT_PASSWORD" \
	--env MYSQL_USER="writefreely" \
	--env MYSQL_PASSWORD="$WRITEFREELY_MYSQL_PASSWORD" \
	--label "io.containers.autoupdate=image" \
	--rm \
	--detach \
	docker.io/library/mariadb:latest
	
# mariadb needs a bit of time to setup
sleep 5

podman run \
	--name "writefreely" \
	--pod "pod-writefreely" \
	--volume "writefreely-data:/go/keys:z" \
	--volume "$HOME/.local/share/containers/storage/volumes/config.ini:/go/config.ini:z" \
	--label "io.containers.autoupdate=image" \
	--rm \
	-it \
	docker.io/writeas/writefreely:latest -c /go/keys/config.ini
